FROM tomcat:8.5.43-jdk11-adoptopenjdk-openj9

RUN  mkdir -p /usr/local/tomcat/appraw \
     && mkdir -p /usr/local/tomcat/webapps \
     && mkdir -p /usr/local/tomcat/logs \
     && mkdir -p /usr/local/tomcat/lib \
     && chmod -R 777 /usr/local/tomcat \
     && chown 1000:1000 -R /usr/local/tomcat

# Add copy jar files to /usr/local/tomcat/webapps/ file.war and /usr/local/tomcat/lib/ file.jar
COPY build/libs/demoGradle-1.0.war /usr/local/tomcat/webapps/demoGradle-1.0.war

EXPOSE 1098
EXPOSE 1099
EXPOSE 8080
